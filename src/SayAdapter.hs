{-# LANGUAGE OverloadedStrings #-}
module SayAdapter where

import Test.Tasty
import Test.Tasty.Hspec

import Say
import Shell

spec_sayCommandInstance :: SpecWith ()
spec_sayCommandInstance =
  describe "Command Say" $ do
    it "Produces say commands" $ do
      command Warning `shouldBe` "say save your work now"
