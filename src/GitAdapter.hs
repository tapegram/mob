{-# LANGUAGE OverloadedStrings #-}
module GitAdapter where

import Test.Tasty ()
import Test.Tasty.Hspec
import Shell
import qualified Git

spec_gitCommandInstance :: SpecWith ()
spec_gitCommandInstance =
  describe "Command Git.Command" $ do
    it "Produces git commands" $ do
      command (Git.Create "test-branch") `shouldBe` "git checkout -b test-branch"

spec_toString :: SpecWith ()
spec_toString =
  describe "toString" $ do
      describe "Create branch" $ do
        it "creates a new branch" $ do
          toString (Git.Create "mob") `shouldBe` "git checkout -b mob"

      describe "Stash" $ do
        it "stash untracked" $ do
          toString (Git.Stash True) `shouldBe` "git stash --include-untracked"
        it "stash" $ do
          toString (Git.Stash False) `shouldBe` "git stash"

      describe "Checkout branch" $ do
        it "checkouts out an existing branch" $ do
          toString (Git.Checkout "mob") `shouldBe` "git checkout mob"

      describe "Add all" $ do
        it "adds all" $ do
          toString Git.AddAll `shouldBe` "git add ."

      describe "Commit all" $ do
        it "commits all with message" $ do
          toString (Git.CommitAll "wip") `shouldBe` "git commit -m \"wip\" -n"

      describe "Push to origin" $ do
        it "Pushes to origin" $ do
          toString (Git.Push "mob") `shouldBe` "git push --no-verify origin mob"

      describe "Branch" $ do
        it "gets branches" $ do
          toString Git.Branch `shouldBe` "git branch"

      describe "DeleteBranch" $ do
        it "deletes a branch" $ do
          toString (Git.DeleteBranch "mob") `shouldBe` "git branch -D mob"

      describe "Pull branch" $ do
        it "pulls a branch" $ do
          toString (Git.Pull "mob") `shouldBe` "git pull origin mob"
