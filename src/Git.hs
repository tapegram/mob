{-# LANGUAGE OverloadedStrings #-}
module Git where

import Test.Tasty ()
import Test.Tasty.Hspec

--------------------------------------------------

type Branch = String
type Origin = Branch
type IncludeUntracked = Bool
type CommitMessage = String

data Command
  = Checkout Branch
  | Create Branch
  | Stash IncludeUntracked
  | AddAll
  | CommitAll CommitMessage
  | Push Origin
  | Branch
  | DeleteBranch Branch
  | Pull Branch
 deriving (Show, Eq)

checkout :: Branch -> Command
checkout branch' = Checkout branch'

createBranch :: Branch -> Command
createBranch branch' = Create branch'

stash :: IncludeUntracked -> Command
stash includeUntracked = Stash includeUntracked

addAll :: Command
addAll = AddAll

commitAll :: CommitMessage -> Command
commitAll message = CommitAll message

push :: Origin -> Command
push origin = Push origin

branch :: Command
branch = Branch

deleteBranch :: Branch -> Command
deleteBranch branch' = DeleteBranch branch'

--------------------------------------------------

spec_git :: SpecWith ()
spec_git =
  describe "Git" $ do

    describe "Git commands" $ do

      describe "createBranch" $ do
        it "creates a new branch" $ do
          createBranch "mob" `shouldBe` Create "mob"

      describe "checkout" $ do
        it "checks out a branch" $ do
          checkout "mob" `shouldBe` Checkout "mob"

      describe "push" $ do
        it "pushs to worksteam" $ do
          push "mob" `shouldBe` Push "mob"

      describe "add all" $ do
        it "adds all" $ do
          addAll `shouldBe` AddAll

      describe "commit all" $ do
        it "commit all with message" $ do
          commitAll "wip" `shouldBe` CommitAll "wip"

      describe "branch" $ do
        it "get branches" $ do
          branch `shouldBe` Branch

      describe "delete branch" $ do
        it "deletes a brnach" $ do
          deleteBranch "mob" `shouldBe` DeleteBranch "mob"

      describe "stash" $ do
        it "stash without untracked" $ do
          stash False `shouldBe` Stash False
        it "stash with untracked" $ do
          stash True `shouldBe` Stash True
