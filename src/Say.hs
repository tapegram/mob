{-# LANGUAGE OverloadedStrings #-}
module Say where

data Say
  = Warning
  | TurnEnded
  | TurnStarted
