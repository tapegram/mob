{-# LANGUAGE OverloadedStrings #-}
module Util where

import Test.Tasty ()
import Test.Tasty.Hspec

import Data.ByteString.Lazy (ByteString)
import Data.ByteString.Lazy.Char8 (unpack)
import Data.List (isPrefixOf)


(|>) :: a -> (a -> b) -> b
(|>) x f = f x
infixl 0 |>


stripLeadingSpaces :: String -> String
stripLeadingSpaces = dropWhile (== ' ')

spec_stripLeadingSpaces :: SpecWith ()
spec_stripLeadingSpaces = describe "Strip spaces" $ do
  it "removes leading spaces" $ do
    stripLeadingSpaces "  test" `shouldBe` "test"

gusBranches :: ByteString -> [String]
gusBranches =
  filter ("gus" `isPrefixOf`)
  . fmap stripLeadingSpaces
  . lines . unpack

spec_gusBranches :: SpecWith ()
spec_gusBranches = describe "gusBranches" $ do
  it "finds branches that start with gus" $ do
    gusBranches "  gus/spiff-up-commands-a-little\n* master\n  mob/update-commands\n  tavish/more_git_types\n"
      `shouldBe`
      ["gus/spiff-up-commands-a-little"]

  it "finds branches that start with gus (multiple)" $ do
    gusBranches "  gus/spiff-up-commands-a-little\n* master\n  mob/update-commands\ngus/more_git_types\n"
      `shouldBe`
      ["gus/spiff-up-commands-a-little", "gus/more_git_types"]

--
-- Helpers for dealing w/ microseconds for things like threadDelay
--
secondsToMicroseconds :: Integral a => a -> a
secondsToMicroseconds = (* 1000000)

minutesToSeconds :: Integral a => a -> a
minutesToSeconds = (* 60)

seconds :: Integral a => a -> a
seconds = secondsToMicroseconds

minutes :: Integral a => a -> a
minutes = secondsToMicroseconds . minutesToSeconds
