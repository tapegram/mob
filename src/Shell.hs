{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE Rank2Types #-}
module Shell where

import qualified Git
import qualified Say

class Command a where
  command :: a -> String

instance Command Git.Command where
  command cmd = toString cmd

toString :: Git.Command -> String
toString c = case c of
  Git.Create branch'       -> "git checkout -b " ++ branch'
  Git.Stash True           -> "git stash --include-untracked"
  Git.Stash False          -> "git stash"
  Git.Checkout branch'     -> "git checkout " ++ branch'
  Git.AddAll               -> "git add ."
  Git.Branch               -> "git branch"
  Git.CommitAll message    -> "git commit -m \"" ++ message ++ "\" -n"
  Git.Push origin          -> "git push --no-verify origin " ++ origin
  Git.DeleteBranch branch' -> "git branch -D " ++ branch'
  Git.Pull branch'         -> "git pull origin " ++ branch'

instance Command Say.Say where
  command cmd =
    let
      text = case cmd of
        Say.Warning     -> "save your work now"
        Say.TurnEnded   -> "turn ended"
        Say.TurnStarted -> "turn started"
    in
      "say " <> text
