# Gus 
A terminal based tool to facilitate mob programming.

![image.png](./image.png)

## Setup

You'll need to install [stack](https://docs.haskellstack.org/en/stable/README/), a build tool for haskell.

From their instructions, run:

`curl -sSL https://get.haskellstack.org/ | sh`

Then in this repo run:

`stack install` 

This will put the built application somewhere on your computer, likely `~/.local/bin`.  If you wish to start the application in the terminal by just typing `gus`, add the location to your `$PATH`.

Note if you are using fish shell, you may wish to install this add on to automatically handle adding haskell things to your `PATH`

`https://github.com/halostatue/fish-haskell`

## Use

Starting the application will present you with a small terminal UI.

The workflow currently implemented goes like this

1. When you hit return on the "Start" command, it will
    - Start a timer for 10 minutes
    - Use git to checkout the branch specified by the configured workstream
2. When the timer is up, it will
    - Commit all the changes you've made
    - Push them to origin
    - Put you back onto the master branch

It will run the git commands based on where you started `gus` from.  So if you are working on kronos, make sure to start `gus` while in the kronos directory.

You can change the workstream by hitting enter on the "Configure" command.

Hitting the "Pause" command will pause the timer, and it can be restarted by hitting "Start".

## Running tests

`stack test`

## Running the app

`stack run`
