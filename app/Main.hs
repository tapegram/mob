{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Main where

-- software transactional memory,
-- ie data stuctures for safely using/passing data
-- between threads
import Control.Monad.STM
import Control.Concurrent.STM.TChan
import Control.Concurrent (forkIO, threadDelay)

-- running things
import Control.Monad (void, forever)
import Data.Maybe

-- our stuff
import UI (runUI, SupportedCommands(..))
import Server.EventHandler (eventLoop, State(..))
import Config
import Util

------------------------------------------------------------------------
-- Main                                                               --
--                                                                    --
-- You'll find server events and handling of commands in the          --
-- Server folder.                                                     --
--                                                                    --
-- UI is still one large UI file.                                     --
------------------------------------------------------------------------

defaultConfig :: Config
defaultConfig = Config
  { _turnLength = 600
  , _stream     = "gus"
  }

main :: IO ()
main = do
  -- the UI will write commands selected or other events here, and
  -- then we can decide what to do with them
  --
  -- it's currently just reusing the supported commands data constructor
  -- / type from the UI but it's likely we'll want different information
  uiEvents' <- atomically (newTChan :: STM (TChan SupportedCommands))

  -- choose either the read config or default
  config' <- fromMaybe defaultConfig <$> readConfig

  -- we give the UI starting config and
  -- uiEvents to to talk to us,
  -- and it gives us toUI to talk to it
  toUI <- runUI config' uiEvents'

  -- make sure the timer is up to date & sync'd
  void $ forkIO $ forever $ do
    atomically $ writeTChan uiEvents' CheckTimerCommand
    threadDelay (seconds 1)

  let initialState = State
        { _timer        = Nothing
        , _config       = config'
        }

  eventLoop toUI uiEvents' initialState
