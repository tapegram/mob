{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
module Config where

import System.Directory
import GHC.Generics
import Data.Text
import Data.Aeson
import Data.Time.Clock
import qualified Data.ByteString.Lazy as B
import Lens.Micro.TH

data Config = Config {
  _stream :: Text
  , _turnLength :: NominalDiffTime
  } deriving (Generic, Show)

makeLenses ''Config

instance ToJSON Config where

instance FromJSON Config where


writeConfig :: Config -> IO ()
writeConfig config = do
  path <- getAppUserDataDirectory "gus"
  B.writeFile path $ encode config

safeReadFile :: FilePath -> IO B.ByteString
safeReadFile fp = do
  exists <- doesFileExist fp
  case exists of
    True  -> B.readFile fp
    False -> pure B.empty

readConfig :: IO (Maybe Config)
readConfig = do
  path <- getAppUserDataDirectory "gus"
  decode <$> safeReadFile path
