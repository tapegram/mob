{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Server.EventHandler where

-- software transactional memory,
-- ie data stuctures for safely using/passing data
-- between threads
import Control.Monad.STM
import Control.Monad.IO.Class
import Control.Concurrent.STM.TChan
import Control.Concurrent (forkIO)
import Brick.BChan (BChan, writeBChan)

-- running things
import Control.Monad (void)
import System.Exit
import Data.Time.Clock
import Data.Text
import Data.ByteString.Lazy (ByteString)
import Lens.Micro
import Lens.Micro.TH

-- testing
import Test.Tasty ()
import Test.Tasty.Hspec
import qualified Control.Monad.State as ST
import Data.Time.Calendar.Julian

-- our stuff
import UI (SupportedCommands(..), ServerEvent(..))
import qualified UI as UI
import Config
import Service
import Util

--------------------------
-- State for the server --
--------------------------
type TimeRemaining = NominalDiffTime
type EndTime = UTCTime
type Seconds = Int

data Timer
  = Paused TimeRemaining
  | Warned TimeRemaining EndTime
  | Ticking TimeRemaining EndTime

data State = State
  { _timer        :: Maybe Timer
  , _config       :: Config
  }

makeLenses ''State

----------
-- Util --
----------
getStream :: State -> Workstream
getStream state = unpack $ state ^. config ^. Config.stream

forkIO_ :: IO () -> IO ()
forkIO_ = void . forkIO

restoreTimer :: UTCTime -> TimeRemaining -> Timer
restoreTimer currentTime timeRemaining =
  Ticking timeRemaining (addUTCTime timeRemaining currentTime)

newTurnTimer :: UTCTime -> NominalDiffTime -> Timer
newTurnTimer currentTime turnLength' =
  let
    endTime       = addUTCTime turnLength' currentTime
    timeRemaining = diffUTCTime endTime currentTime
  in
    Ticking timeRemaining endTime

type WrappedRunner m
  =  Workstream
  -> ParserCommand
  -> ([Either ByteString ByteString] -> m ())
  -> m ()

data Env m = Env
  { runner :: WrappedRunner m
  , writeUI :: ServerEvent -> m ()
  , now :: UTCTime
  }

realRunner :: WrappedRunner IO
realRunner workstream command fn =
  void $ forkIO $ handle workstream command >>= fn

------------------------------------------------
-- The main portion, handles and emits events --
------------------------------------------------
eventHandler :: MonadIO m => Env m -> SupportedCommands -> State -> m (State)
eventHandler env event state =
  let sendUI         = writeUI env -- writeBChan (state ^. serverEvents)
      commandHandler = runner env
      currentTime    = now env

  in case event of

    StartCommand -> case state ^. timer of

      Just (Paused timeRemaining) -> do
        LogEvent "Continuing..."
          |> sendUI

        let restoredTimer = restoreTimer currentTime timeRemaining

        state
          |> timer .~ Just restoredTimer
          |> pure

      -- if there's no timer, we're starting!
      _ -> do
        sendUI $ LogEvent "Starting..."

        commandHandler (getStream state) Start $ \_ ->  pure ()

        let timer' = newTurnTimer
              currentTime
              (state ^. config ^. Config.turnLength)

        state
          |> (timer .~ Just timer')
          |> pure

    PauseCommand ->
      let
        timeRemaining = case state ^. timer of
          Just (Ticking timeRemaining' _) -> timeRemaining'
          Just (Paused t)                 -> t
          Just (Warned t _)               -> t
          Nothing                         -> 0
        pausedTimer = Just $ Paused timeRemaining
      in
        state
          |> timer .~ pausedTimer
          |> pure

    CheckTimerCommand -> do

      case state ^. timer of

        Just (Ticking _ endTime) -> do
          let timeRemaining = diffUTCTime endTime currentTime

          if timeRemaining < 30
            then do
              sendUI $ LogEvent "Save your work!"
              commandHandler (getStream state) Warn $ \result -> do
                show result
                  |> LogEvent
                  |> sendUI
            else
              show timeRemaining
                |> UI.Remaining
                |> TimerEvent
                |> sendUI

          let newTimer
                | timeRemaining < 30 = Warned timeRemaining endTime
                | otherwise          = Ticking timeRemaining endTime

          state
            |> timer .~ Just newTimer
            |> pure

        Just (Warned _ endTime) -> do
          let timeRemaining = diffUTCTime endTime currentTime
              newTimer
                | timeRemaining < 0 = Nothing
                | otherwise         = Just $ Warned timeRemaining endTime

          if timeRemaining < 0
            then do
              LogEvent "Time up!"
                |> sendUI

              commandHandler (getStream state) End $ \result -> do
                show result
                  |> LogEvent
                  |> sendUI
            else do
              show timeRemaining
                |> UI.Remaining
                |> TimerEvent
                |> sendUI

          state
            |> timer .~ newTimer
            |> pure

        Nothing -> do
          TimerEvent UI.Finished |> sendUI
          pure state
        _ -> do
          TimerEvent UI.Paused |> sendUI
          pure state

    ChangedConfig newConfig ->
      pure (config .~ newConfig $ state)

    ConfigureCommand ->
      pure state -- noop

    QuitCommand -> do
      liftIO $ writeConfig (state ^. config)
      liftIO $ exitSuccess

eventLoop :: BChan ServerEvent -> TChan SupportedCommands -> State -> IO ()
eventLoop toUIChan uiEvents state = do
  uiEvent <- atomically $ readTChan uiEvents
  currentTime <- getCurrentTime

  let env = Env
        { runner  = realRunner
        , writeUI = writeBChan toUIChan
        , now     = currentTime
        }

  newState <- eventHandler env uiEvent state
  eventLoop toUIChan uiEvents newState


pureEnv :: Env (ST.StateT [String] IO)
pureEnv = Env
  { runner = \ws cmd -> \_result -> do
      ST.modify (<> ["Shell ws: " <> ws <> " cmd: " <> show cmd])

  , writeUI = \event -> do
      ST.modify (<> ["UI: " <> show event])

  , now     = UTCTime (fromJulianYearAndDay 500 0) 0
  }

spec_eventHandler :: SpecWith ()
spec_eventHandler = do
  let
    config' = Config
      { _turnLength = 600
      , _stream = "test"
      }
    state = State
      { _timer = Nothing
      , _config = config'
      }

  describe "ChangedConfig" $ do
    it "Does nothing" $ do
      result <- ST.execStateT (eventHandler pureEnv ConfigureCommand state) []
      result `shouldBe` []

  describe "StartCommand" $ do
    it "Tells the user it's starting" $ do
      result <- ST.execStateT (eventHandler pureEnv StartCommand state) []
      result
        `shouldBe`
        [ "UI: LogEvent \"Starting...\""
        , "Shell ws: test cmd: Start"
        ]
