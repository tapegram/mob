{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE OverloadedStrings #-}
module Service where

import Data.ByteString.Lazy (ByteString)
import Data.Semigroup ((<>))
import Data.String (fromString)
import System.Exit (ExitCode(ExitFailure))
import System.Process.Typed
import qualified Git
import qualified GitAdapter ()
import qualified Shell
import qualified Say
import qualified SayAdapter ()


------------------------------------------
-- The main entry point for the service --
------------------------------------------

type Runner m
     =  Workstream
     -> ParserCommand
     -> m ([Either ByteString ByteString])

handle :: Runner IO
handle workstream command =
  let forGusCommand = case command of
        Start               -> StartWorkstream
        Next message        -> HandOff message
        End                 -> EndTurn
        Warn                -> WarnTurn
        -- drop out of existence if we receive unhandled command
        _                   -> error "Unsupported Command"
  in
    trainedGus workstream forGusCommand

----------------------------------------------------------------------------------------------------
-- The gus service

type Workstream = String
type MasterBranch = String

data GusCommand
  = StartWorkstream
  | HandOff CommitMessage
  | EndTurn
  | WarnTurn

gus :: (forall a. Shell.Command a => a -> IO (Either ByteString ByteString))
    -> MasterBranch
    -> Workstream
    -> GusCommand
    -> IO ([Either ByteString ByteString])
gus runner masterBranch workstream StartWorkstream = do
  _ <- mapM runner
    [ (Git.stash True)
    , (Git.checkout masterBranch)
    ]

  branchExists <- runner $ Git.checkout workstream

  case branchExists of
    -- if the branch doesn't already exist, create and checkout
    Left _ -> do
      mapM runner
        [ Git.createBranch workstream
        , Git.Pull workstream
        ]
    -- or, just pull since we're on it
    Right _ -> do
      fmap (:[]) $ runner $ Git.Pull workstream

gus runner masterBranch workstream (HandOff commitMessage) = do
  mapM runner
    [ (Git.AddAll)
    , (Git.CommitAll commitMessage)
    , (Git.Push workstream)
    , (Git.Checkout masterBranch)
    , (Git.DeleteBranch workstream)
    ]

gus runner masterBranch workstream EndTurn = do
  _ <- runner Say.TurnEnded
  gus runner masterBranch workstream (HandOff "wip")

gus runner _masterBranch _workstream WarnTurn = do
  _ <- runner Say.Warning
  pure mempty

----------------------------------------------------------------------------------------------------
-- CLI/Parser types and setup

type Name = String
type CommitMessage = String
type TicketNumber = String

data ParserCommand
  = Start
  | StartWith TicketNumber
  | Next CommitMessage
  | Continue
  | ContinueWith TicketNumber
  | Done
  | End
  | Warn
  deriving Show

----------------------------------------------------------------------------------------------------
-- Adapters and implementations

runInShell :: String -> IO (Either ByteString ByteString)
runInShell s = fmap toEither $ readProcess . toProcessConfig $ s

shellRunner :: Shell.Command a => a -> IO (Either ByteString ByteString)
shellRunner = runInShell . Shell.command

trainedGus :: Workstream -> GusCommand -> IO ([Either ByteString ByteString])
trainedGus workstream = gus shellRunner "master" workstream

toEither :: (ExitCode, ByteString, ByteString) -> Either ByteString ByteString
toEither (ExitFailure _, stdOut, errOut ) = Left $ stdOut <> errOut
toEither (_            , ""    , error' ) = Left error'
toEither (_            , output, ""     ) = Right output
toEither (_code        , stdOut, errOut ) = Right $ stdOut <> " " <> errOut

toProcessConfig :: String -> ProcessConfig () () ()
toProcessConfig input = fromString input
