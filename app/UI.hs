{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module UI where

import Brick
import Brick.BChan
import Brick.Forms
import Brick.Focus
import qualified Brick.Main             as M
import qualified Brick.Types            as T
import qualified Brick.Focus            as F
import qualified Brick.Widgets.Center   as C
import qualified Brick.Widgets.Edit     as E
import qualified Brick.AttrMap          as A
import qualified Brick.Widgets.Border   as B
import qualified Brick.Widgets.List     as L
import qualified Graphics.Vty           as V
import           Brick.Widgets.Core     (vBox)
import           Lens.Micro
import           Lens.Micro.TH
import qualified Data.Vector            as Vec
import           Data.Maybe             (fromJust)
import           Data.Text              (unpack)
import           Data.List              (intersperse)
import           Control.Concurrent     (forkIO)
import           Control.Monad.IO.Class (liftIO)
import           Control.Monad.STM
import           Control.Monad          (void)
import           Control.Concurrent.STM.TChan

import           Config
import           Util

--
-- The overall architecture of this is a lot like redux, with a global
-- State being applied to a View, and state changes triggering a redraw.
--
-- Keyboard input triggers something akin to Actions, which can change
-- State.
--

--
-- quick note on lenses:
--
-- the fn ^. is for access, so if we had state and wanted the mob name
-- from config, we would do state ^. config ^. mob, which corresponds to
-- state["config"]["mob"] in python/javascript
--
-- the fn .~ is for setting, so for example if wanted to translate
-- state["config"]["mob"] = "matcha/mob",  we would do
-- config . mob .~ "matcha/mob" $ state
--    creates a setter fn      passes state into the setter,
--                                returns updated state
--

-------------
-- Styling --
-------------

globalDefault :: V.Attr
globalDefault = V.white `on` V.black

customAttr :: AttrName
customAttr = L.listSelectedAttr <> "custom"

progressDoneAttr      :: AttrName
progressDoneAttr      = A.attrName "progressDone"
progressRemainingAttr :: AttrName
progressRemainingAttr = A.attrName "progressRemaining"

greenAttr, redAttr, yellowAttr, blueAttr :: AttrName
greenAttr  = A.attrName "green"
redAttr    = A.attrName "red"
yellowAttr = A.attrName "yellow"
blueAttr   = A.attrName "blue"

mkColor :: Integer -> Integer -> Integer -> V.Color
mkColor = V.rgbColor

yellow, green, red, blue, grey :: V.Color
yellow = mkColor 181 137 0
green  = mkColor 133 154 0
red    = mkColor 220 50  47
blue   = mkColor 38  139 210
grey   = mkColor 147 161 161

styles :: A.AttrMap
styles = A.attrMap globalDefault
  [ (E.editAttr,            V.white `on` V.blue)
  , (E.editFocusedAttr,     V.black `on` grey)
  , (customAttr,            fg V.white)
  , (progressDoneAttr,      V.black `on` V.red)
  , (progressRemainingAttr, V.black `on` V.green)
  , (yellowAttr,            yellow  `on` V.black)
  , (greenAttr,             green `on` V.black)
  , (redAttr,               red `on` V.black)
  , (blueAttr,              blue `on` V.black)
  ]

--------------------------------------
-- Fields or lists you can focus on --
--------------------------------------

data Name
  = StreamField
  | CommandList
  deriving (Eq, Ord, Show)

------------------------------------------
-- Current commands and how they appear --
------------------------------------------

data SupportedCommands
  = StartCommand
  | PauseCommand
  | ConfigureCommand
  | QuitCommand
  | CheckTimerCommand
  | ChangedConfig Config

class Render a where
  render :: a -> Widget Name

instance Render SupportedCommands where
  render StartCommand     = withAttr greenAttr $ str "Start"
  render PauseCommand     = str "Pause"
  render ConfigureCommand = str "Configure"
  render QuitCommand      = withAttr redAttr $ str "Quit"
  render _                = str "Unused"

------------------
-- Global State --
------------------

-- we can add more pages to this like "Login" or w/e
data Page = Main

data Timer
  = Remaining String
  | Paused
  | Finished
  deriving Show

-- here we can extend custom events we'd like to send from Main
-- to the UI
data ServerEvent
  = LogEvent String
  | TimerEvent Timer
  deriving Show

data State = State {
  -- underscore prefixes because that's how lens rolls
  _focusRing      :: F.FocusRing Name
  , _config       :: Config
  , _configForm   :: Form Config ServerEvent Name
  , _showConfig   :: Bool
  , _page         :: Page
  , _commands     :: L.List Name SupportedCommands
  , _outChannel   :: TChan SupportedCommands
  , _eventsLog    :: [String]
  , _timer        :: Maybe String
  }

makeLenses ''State

----------
-- View --
----------

listDrawElement :: Render a => Bool -> a -> Widget Name
listDrawElement selected a =
    let selectedAppearance s =
          if selected
             -- do something fancy with its appearance
          then withAttr customAttr (str "> " <+> UI.render s <+> str " <")
             -- just let it be itself
          else UI.render s
    in C.hCenter $ (selectedAppearance a)

mkConfigForm :: Config -> Form Config e Name
mkConfigForm =
  let label s w = padBottom (Pad 1) $
        (vLimit 1 $ hLimit 15 $ str s <+> fill ' ') <+> w
  in
    newForm
    [
      (label "Stream") @@= editTextField stream StreamField (Just 1)
    ]

mkConfigModal
  :: Form Config ServerEvent Name
  -> T.Widget Name
mkConfigModal configForm' =
  let
    doBorder = B.borderWithLabel (withAttr yellowAttr $ str "Configuration")
  in
    C.centerLayer
    $ hLimitPercent 50
    $ vLimitPercent 75
    $ doBorder
    $ C.hCenter
    $ padAll 1
    $ vBox
    [ renderForm configForm'
    , C.hCenter
      $ hBox
      $ intersperse (str "     ") $ [ withAttr greenAttr $ str "Return - Save"
                                    , withAttr redAttr $ str "Esc - Cancel"
                                    ]
    ]

view :: State -> [T.Widget Name]
view state =
  case state ^. page of
    Main -> [configModal, ui]
      where
        commandList = state ^. commands

        box =
          padAll 1 $
          hLimitPercent 100 $
          vLimitPercent 100 $
          L.renderList listDrawElement True commandList

        config' = withAttr yellowAttr $ str "Config"

        logs =
          padAll 1 $
          hLimit 51 $
          vLimit 15 $
          str (foldr (\s acc -> acc <> "\n" <> s) "" (state ^. eventsLog))

        timerText = case (state ^. timer) of
          Nothing -> str "No Timer Set"
          Just s  -> str s

        configModal =
          if (state ^. showConfig)
          then mkConfigModal (state ^. configForm)
          else str ""

        streamText = unpack $ state ^. config ^. stream

        ui = vBox
          [ padAll 1 $ C.hCenter $ hBox [ withAttr greenAttr $ timerText ]
          , hBox
            [ padAll 1 $ C.center $ vBox [ C.hCenter (withAttr yellowAttr $ str "Commands"), box ]
            , vBox
              [ padAll 1 $ C.center $ hLimit 50 $ vBox
                [ C.hCenter config'
                , C.hCenter $ padAll 1 $ str $ "Workstream: " <> streamText
                , fill ' '
                ]
              , padAll 1 $ C.center $ hLimit 50 $ vBox
                [ C.hCenter (withAttr yellowAttr $ str "Log")
                , logs
                , fill ' '
                ]
              ]
            ]
          ]

-------------------
-- Event Handler --
-------------------

commandSelected :: State -> T.EventM Name (T.Next State)
commandSelected state = do
  let
    -- get elements of command list
    commandList =
      (state ^. commands) ^. L.listElementsL

    -- get selected command index
    selectedCommandIndex =
      fromJust $ (state ^. commands) ^. L.listSelectedL

    -- finally turn it back into a string
    selectedCommand =
      commandList Vec.! selectedCommandIndex
    -- pretty gnarly, I bet there's a simpler way

  -- lil helper
  let writeToServer command =
        liftIO . atomically
        $ writeTChan (state ^. outChannel) command

  case selectedCommand of
    StartCommand -> do
      writeToServer StartCommand
      M.continue state

    PauseCommand -> do
      writeToServer PauseCommand
      M.continue state
    ConfigureCommand ->
      let
        newFocusRing = focusSetCurrent StreamField (state ^. UI.focusRing)
      in
        state
        |> showConfig .~ True
        |> UI.focusRing .~ newFocusRing
        |> M.continue

    QuitCommand -> M.halt state

    _ -> M.continue state

focusedEventHandler
  :: State
  -> BrickEvent Name ServerEvent
  -> Maybe Name
  -> T.EventM Name (T.Next State)
focusedEventHandler state event (Just focused) =
  case focused of
    StreamField -> do
      let
        (T.VtyEvent event') = event
        stateUpdate = case event' of
          V.EvKey V.KEsc [] ->
            (showConfig .~ False)
            . (UI.focusRing .~ focusSetCurrent CommandList
               (state ^. UI.focusRing))
          V.EvKey V.KEnter [] ->
            (showConfig .~ False)
            . (UI.focusRing .~ focusSetCurrent CommandList
               (state ^. UI.focusRing))
            . (config .~ formState (state ^. configForm))
          _ ->
            id

      newFormState <- handleFormEvent event (state ^. configForm)

      let newState = state
            |> configForm .~ newFormState
            |> stateUpdate

      liftIO . atomically
        $ writeTChan
        (newState ^. outChannel)
        (ChangedConfig (newState ^. config))

      M.continue newState
    CommandList -> do
      let
        (T.VtyEvent event') = event
      case event' of
        -- handle when the user hits return on a selected command
        V.EvKey V.KEnter [] ->
          commandSelected state
        _ ->
          -- this is for handling going up/down/hitting return on the
          -- command list
          let
            updatedList = L.handleListEvent event' (state ^. commands)
            newState = (\list -> commands .~ list $ state) <$> updatedList
          in
            newState >>= M.continue
focusedEventHandler state _ _ = M.continue state

eventHandler
  :: State
  -> T.BrickEvent Name ServerEvent
  -> T.EventM Name (T.Next State)
eventHandler state rawEvent@(T.VtyEvent event) =
  -- if we add more pages, we can handle events differently
  case state ^. page of
    Main ->
      case event of
        V.EvKey (V.KChar 'q') [] -> M.halt state

        V.EvKey (V.KChar '\t') [] ->
          let
            nextFocus = focusNext (state ^. UI.focusRing)
          in
            M.continue (UI.focusRing .~ nextFocus $ state)
        V.EvKey V.KBackTab [] ->
          let
            nextFocus = focusPrev (state ^. UI.focusRing)
          in
            M.continue (UI.focusRing .~ nextFocus $ state)
        _ ->
          let
            focus = focusGetCurrent (state ^. UI.focusRing)
          in
            focusedEventHandler state rawEvent focus

-- Here we'll handle any events from the controller
eventHandler state (AppEvent (LogEvent newEvent)) =
  let
    currEventsLog = state ^. eventsLog
    newEvents     = currEventsLog <> [newEvent]
  in
    M.continue (eventsLog .~ newEvents $ state)

eventHandler state (AppEvent (TimerEvent event)) =
  let
    newTimer = case event of
      Paused      -> Just "Timer Paused"
      Remaining s -> Just s
      Finished    -> Nothing
    changeTimerState = timer .~ newTimer
  in
    M.continue (changeTimerState state)

eventHandler state event = do
  newFormState <- handleFormEvent event (state ^. configForm)

  M.continue (configForm .~ newFormState $ state)

----------
-- Main --
----------

-- wires everything up
app :: M.App State ServerEvent Name
app = M.App
  { M.appDraw = view
  , M.appChooseCursor = M.showFirstCursor
  , M.appHandleEvent = eventHandler
  , M.appStartEvent = return
  , M.appAttrMap = const styles
  }

supportedCommands :: L.GenericList Name Vec.Vector SupportedCommands
supportedCommands =
  (L.list CommandList
   $ Vec.fromList
   [StartCommand, PauseCommand, ConfigureCommand, QuitCommand])
  0

-- run it
runUI :: Config -> (TChan SupportedCommands) -> IO (BChan ServerEvent)
runUI config' outChannel' = do
  -- pretty much the same deal as a TChan, just brick specific
  eventChan <- Brick.BChan.newBChan 10

  let buildVty = V.mkVty V.defaultConfig
  initialVty <- buildVty

  let initialState = State
        { _focusRing  = F.focusRing [CommandList, StreamField]
        , _config     = config'
        , _configForm = mkConfigForm config'
        , _showConfig = False
        , _page       = Main
        , _commands   = supportedCommands
          -- we throw the outChannel into state so that we can
          -- write to it from the eventHandler / event handler
        , _outChannel = outChannel'
        , _eventsLog  = []
        , _timer      = Nothing
        }

  -- we fork here so that runUI isn't stuck evaluating until the UI
  -- is exited (so that we can get at the eventChan)
  void $ forkIO $ do
    _endState <-
      M.customMain initialVty buildVty (Just eventChan) app initialState
    -- since the end state is returned when the UI exits,
    -- we write a final message up the chain to do any
    -- cleanup
    atomically $ writeTChan outChannel' QuitCommand

  pure eventChan
